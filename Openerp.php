<?php

set_include_path(get_include_path() . PATH_SEPARATOR . dirname(__FILE__) . '/openerp/lib/');

require_once 'Zend/XmlRpc/Client.php';

class CI_Openerp {

    public $server;
    public $user;
    public $password;
    public $database;
    public $modelsPath;

    public function __construct() {
        $this->server = config_item('server');
        $this->user = config_item('adminUser');
        $this->password = config_item('adminPass');
        $this->database = config_item('db');
        log_message('debug', "OpenERP Class Initialized");
    }

    // --------------------------------------------------------------------

    /**
     * Initialize preferences
     *
     * @access	public
     * @param	array
     * @return	void
     */
    private $_userId;

    public function getUserId() {
        if ($this->_userId == null) {
            $this->_userId = $this->getClientCommon()->login($this->database, $this->user, $this->password);
        }

        return (int) $this->_userId;
    }

    private $_clientCommon;

    public function getClientCommon() {
        if ($this->_clientCommon == null) {
            $conn = new Zend_XmlRpc_Client($this->server . '/common');
            $this->_clientCommon = $conn->getProxy();
        }
        return $this->_clientCommon;
    }

    private $_client;

    public function getClient() {
        if ($this->_client == null) {
            $conn = new Zend_XmlRpc_Client($this->server . '/object');
            $this->_client = $conn->getProxy();
        }
        return $this->_client;
    }

    /**
     * @return new id data
     */
    public function create($model, array $values, $context = null) {
        return $this->getClient()->execute($this->database, $this->getUserId(), $this->password, $model, 'create', $values, $context);
    }

    /**
     * @return array id data
     */
    public function search($model, array $args = array(), $offset = 0, $limit = null, $order = null, array $context = null, $count = false) {
        return $this->getClient()->execute($this->database, $this->getUserId(), $this->password, $model, 'search', $args, $offset, $limit, $order, $context, $count);
    }

    /**
     * return array
     */
    public function read($model, array $ids, array $fields = null, $context = null) {
        return $this->getClient()->execute($this->database, $this->getUserId(), $this->password, $model, 'read', $ids, $fields, $context);
    }

    /**
     * @return ..
     */
    public function write($model, array $ids, array $values, $context = null) {
        return $this->getClient()->execute($this->database, $this->getUserId(), $this->password, $model, 'write', $ids, $values, $context);
    }

    /**
     * @return ..
     */
    public function unlink($model, array $ids, $context = null) {
        return $this->getClient()->execute($this->database, $this->getUserId(), $this->password, $model, 'unlink', $ids, $context);
    }

    private $_metaData;

    public function getMetaData($model) {
        if (!isset($this->_metaData[$model])) {
            $args =  array(
                array('model', '=', $model),
            );
            $fieldtoshow = array('name', 'ttype', 'relation', 'relation_field',
                'field_description', 'required');
            
            $results = $this->searchRead('ir.model.fields', $args, $fieldtoshow, NULL, NULL, 'name ASC');
            
            foreach ($results as $afield) {
                $fields[$afield['name']] = array(
                    'required' => $afield['required'],
                    'type' => $afield['ttype'],
                    'label' => $afield['field_description'],
                    'relationModel' => $afield['relation'],
                    'foreignKeyField' => $afield['relation_field'],
                );
            }
            $this->_metaData[$model] = $fields;
        }
        return $this->_metaData[$model];
    }

    public function query($model, $criteria, $count = false) {
        $ids = $this->search($this->getUserId(), $this->password, $model, $criteria->domain, $criteria->offset, $criteria->limit, $criteria->order, $criteria->context, $count);
        if (!is_array($ids)) {
            return $ids;
        }
        return $this->read($model, $ids, $criteria->fields, $criteria->context);
    }

    function searchRead($model = '', $args = array(), $fields = array(), $offset = NULL, $limit = NULL, $order = NULL, $context = array(), $count = false) {
        try {
            $ids = $this->search($model, $args, $offset, $limit, $order, $context, $count);
        } catch (Exception $exc) {
            echo $exc->getMessage();
            exit();
        }

        $result = $this->read($model, $ids, $fields, $context);
        return $result;
    }

    function wkf($model = '', $wf = '', $id = 0) {
        return $this->getClient()->exec_workflow($this->database, $this->getUserId(), $this->password, $model, $wf, $id);
    }

    private $_report;

    public function getReport() {
        if ($this->_report == null) {
            $conn = new Zend_XmlRpc_Client($this->server . '/report');
            $this->_report = $conn->getProxy();
        }
        return $this->_report;
    }

    public function call_function($model, $function, $ids, $params) {
        return $this->getClient()->execute($this->database, $this->getUserId(), $this->password, $model, $function, $ids, $params);
    }

    function report($model = '', $object = '', $ids = array(), $params1 = array(), $params2 = array()) {
        $res_id = $this->getReport()->report($this->database, $this->getUserId(), $this->password, $model, $ids, $params1, $params2);
        (int) $res_id;
        sleep(5);
        $state = FALSE;
        $attemp = 0;
        while (!$state) {
            $raw = $this->_rawReport($res_id);
            $state = $raw['state'];
            if (!$state) {
                sleep(1);
                $attempt += 1;
            }
            if ($attemp > 200) {
                print 'Printing aborted, too long delay !';
            }
        }
        return $raw['result'];
    }

    function _rawReport($res_id = 0) {
        $report = $this->getReport()->report_get($this->database, $this->getUserId(), $this->password, $res_id);
        return $report;
    }

    function traverse_structure($ids) {

        $return_ids = array();
        $iterator = new RecursiveArrayIterator($ids);
        while ($iterator->valid()) {
            if ($iterator->hasChildren()) {
                $return_ids = array_merge($return_ids, $this->traverse_structure($iterator->getChildren()));
            } else {
                if ($iterator->key() == 'int') {
                    $return_ids = array_merge($return_ids, array($iterator->current()));
                }
            }
            $iterator->next();
        }
        return $return_ids;
    }

}